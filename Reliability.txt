Reliability:

1) The System should not crash more than once in a year.
2) The System should not fail more than once when uploading data during 3 months (provided theres enough space and file is not corrupted).
3) The System should not fail to update the weather, ice status and Earth's magnetic field more than once a month.
